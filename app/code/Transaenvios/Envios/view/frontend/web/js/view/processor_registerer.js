define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/shipping-rate-service',
        %custom shipping rate processor%,
        'Magento_Checkout/js/model/shipping-save-processor',
        %custom shipping save processor%
    ],
    function (
        Component,
        shippingRateService,
        customShippingRateProcessor,
        shippingSaveProcessor,
        customShippingSaveProcessor
    ) {
        'use strict';
 
        /** Register rate processor */
        shippingRateService.registerProcessor(%address type%, customShippingRateProcessor);
 
        /** Register save shipping address processor */
        shippingSaveProcessor.registerProcessor(%address type%, custormShippingSaveProcessor);
 
        /** Add view logic here if needed */
        return Component.extend({});
    }
);